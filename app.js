'use strict';

const Homey = require('homey');
const fetch = require('fetch-everywhere');
const ModeHandler = require('./src/utils/ModeHandler');
const settings = Homey.ManagerSettings;

class MyApp extends Homey.App {
  onInit() {
    this.log('Application is running...');

    this.modeHandler = new ModeHandler();
    this.callAction = this.callAction.bind(this);
    this.setupAction();
  }

  setupAction() {
    this.log('Registering action');

    new Homey.FlowCardAction('vibrate_smartwatch')
      .register()
      .registerRunListener(this.callAction);
  }

  callAction(args, state) {
    this.log('Action is triggered');

    const apiUrl = this.getApiUrl();
    const userId = this.getUserId();

    // make sure userid is set.
    if (!userId && userId === undefined) {
      // TODO: Make homey talk and show it on the led ring.
      return;
    }

    const { text, vibration, color } = args;

    return fetch(`${apiUrl}/graphql`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        { 
          sendMessage(input: 
            { 
              _id: "${userId}"
              text: "${text}"
              vibration: "${vibration}"
              color: "${color}"
            }
          )
        }`
      })
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        // TODO: When there is an error show it on the homey
        // TODO: When everything is okay make the homey blink
        // this.modeHandler.set('success');
        console.log('Succes');
        console.log(res);
        return Promise.resolve();
      })
      .catch((err) => {
        // this.modeHandler.set('error');
        console.log('error', err);
        return Promise.reject();
      });
  }

  getApiUrl() {
    if (settings.get('apiUrl')) {
      return settings.get('apiUrl');
    }
    return 'https://haptics.bartimeus.nl';
  }

  getUserId() {
    return settings.get('userId');
  }
}

module.exports = MyApp;
