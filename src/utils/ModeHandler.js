const Homey = require('homey');

class ModeHandler {
  constructor() {
    this.set = this.set.bind(this);
  }

  setupErrorMode() {
    let frames = [];
    let frame = [];

    for (let pixelIndex = 0; pixelIndex < 24; pixelIndex++) {
      if (pixelIndex < 1) {
        frame.push({
          r: 255,
          g: 0,
          b: 0
        });
      } else {
        frame.push({
          r: 0,
          g: 0,
          b: 0
        });
      }
    }
    frames.push(frame);

    new Homey.LedringAnimation({
      options: {
        fps: 1, // real frames per second
        tfps: 60, // target frames per second. this means that every frame will be interpolated 60 times
        rpm: 16 // rotations per minute
      },
      frames: frames,
      duration: 10000
    })
      .register()
      .then((animation) => {
        this.errorMode = animation;
      })
      .catch((err) => {
        console.error(err);
      });
  }

  setupSuccessMode() {
    let frames = [];
    let frame = [];

    for (let pixelIndex = 0; pixelIndex < 24; pixelIndex++) {
      if (pixelIndex < 1) {
        frame.push({
          r: 0,
          g: 255,
          b: 0
        });
      } else {
        frame.push({
          r: 0,
          g: 0,
          b: 0
        });
      }
    }
    frames.push(frame);

    new Homey.LedringAnimation({
      options: {
        fps: 1, // real frames per second
        tfps: 60, // target frames per second. this means that every frame will be interpolated 60 times
        rpm: 16 // rotations per minute
      },
      frames: frames,
      duration: 5000
    })
      .register()
      .then((animation) => {
        this.succesMode = animation;
      })
      .catch((err) => {
        console.error(err);
      });
  }

  /**
   * This function changes the mode of the homey
   *
   * error - led strip will blink red.
   *
   * @param  {} mode
   */
  set(mode) {
    switch (mode) {
      case 'error': {
        if (this.errorMode) {
          this.errorMode.start();
        }
      }
      case 'success': {
        if (this.succesMode) {
          this.succesMode.start();
        }
      }
    }
  }
}

module.exports = ModeHandler;
